var dir1 = [], cutName = [], click;
$.ajax({
  url: './php/main.php',
  method: 'POST',
  data: { method: 'get' },
  dataType: "json",
  success: function (response) {
    for ($i = 0; $i < (response.data.files.length); $i++) {//perform when files are exist on root directory
      var ext = response.data.files[$i].name.split('.')[1];
      $(".sub-panel").append('<div class="parent"><button class="children" data-path= "../root" data-text= "' + response.data.files[$i].name + '"><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
      $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "../root" data-text=' + response.data.files[$i].name + '><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
    }
    for ($i = 0; $i < (response.data.folder.length); $i++) {//perform when folders are exist on root directory
      $(".sub-panel").append('<div class="parent"><button class="children" data-path= "../root" data-text=' + response.data.folder[$i].name + '><img src="./assets/images/folder.png" alt="">' + response.data.folder[$i].name + '</button><div class="sub-panel-2"></div></div>');
      $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "../root" data-text=' + response.data.folder[$i].name + '><img src="./assets/images/folder.png" alt="">' + response.data.folder[$i].name + '</button><div class="sub-panel-2"></div></div>');
    }
  }
});

function show(filename, dir) {//call when we need to make empty dir and get new data
  $.ajax({
    url: "./php/main.php",
    data: { method: 'check', filename1: filename, dir: dir },
    method: "POST",
    dataType: "json",
    success: function (response) {
      if (response.data.files.length == 0) {// if files have nothing within than it will only update path
        $(".rigthContent").attr('data-path', response.mainpath);
      } else {
        for ($i = 0; $i < (response.data.files.length); $i++) {
          var ext = response.data.files[$i].name.split('.')[1];
          $(".rigthContent").attr('data-path', response.mainpath).append('<div class="parent"><button class="children1" data-path= ' + response.mainpath + ' data-text=' + response.data.files[$i].name + '><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
      }
      if (response.data.folder.length == 0) {// if folder have nothing within than it will only update path
        $(".rigthContent").attr('data-path', response.mainpath)
      } else {
        for ($i = 0; $i < (response.data.folder.length); $i++) {
          $(".rigthContent").attr('data-path', response.mainpath).append('<div class="parent"><button class="children1" data-path= ' + response.mainpath + ' data-text=' + response.data.folder[$i].name + '><img src="./assets/images/folder.png" alt="">' + response.data.folder[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
      }
    }
  });
}

$(document).on('click', '.children', function () {//maily append on left panel and toggle class
  var openPath = $(this).attr("data-path");
  $(".rigthContent").children().remove();
  var filename = $(this).text();
  show(filename, openPath);
  $(this).next().addClass('test');
  var parentEle = $(this).next();
  $(parentEle).children().remove();
  var dir = $(this).attr('data-path');
  $(this).next().toggleClass('active').parent().siblings().children().removeClass('active');//toggle our left panel
  $.ajax({
    url: "./php/main.php",
    data: { method: 'check', filename1: filename, dir: dir },
    method: "POST",
    dataType: "json",
    success: function (response) {
      if (response.data.files.length == 0) {// if files have nothing within than it will only update path
        $(".rigthContent").attr('data-path', response.mainpath);
      } else {
        for ($i = 0; $i < (response.data.files.length); $i++) {
          var ext = response.data.files[$i].name.split('.')[1];
          $(parentEle).append('<div class="parent"><button class="children" data-path= ' + response.mainpath + ' data-text=' + response.data.files[$i].name + '><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
      }
      if (response.data.folder.length == 0) {
        $(".rigthContent").attr('data-path', response.mainpath);
      } else {
        for ($i = 0; $i < (response.data.folder.length); $i++) {
          $(parentEle).append('<div class="parent"><button class="children" data-path= ' + response.mainpath + ' data-text=' + response.data.folder[$i].name + '><img src="./assets/images/folder.png" alt="">' + response.data.folder[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
      }
    }
  });
});

$(document).on('dblclick', '.children1', function () {//dblclick to enter the folder
  $('.left').find('.deletethis').removeClass('deletethis');//this will remove the addclass
  var CommonText = $(this).eq($(this).index()).attr("data-text");//this will add delete class on the left side
  var leftSync = $('.left').find("[data-text ='" + CommonText + "']:last");
  $(leftSync).trigger("click");
});

$(document).on("click", ".back", function () {
  $('.active:last').removeClass('active');
  var filename = $(this).text();
  var oldpath = $(".rigthContent").attr('data-path');
  var split = oldpath.split("/");// here will split the path and remove the last dir/file so we can go back easily
  split = split.splice(0, split.length - 1);
  var dir = split.join("/");
  if (dir == "..") {    // check wether this is the end of our dir
    bootbox.alert({
      message: "This is the end",
      backdrop: true
    });
    dir = "..";
  }
  if (dir != "..") {// if this is not the end than enter
    $(".rigthContent").children().remove();// remove all the elements of current folder
    $.ajax({
      url: "./php/back.php",
      data: { method: 'back', filename1: filename, dir: dir },
      method: "POST",
      dataType: "json",
      success: function (response) {//append all the elements of new path
        for ($i = 0; $i < (response.data.files.length); $i++) {
          var ext = response.data.files[$i].name.split('.')[1];
          $(".rigthContent").attr('data-path', response.mainpath).append('<div class="parent"><button class="children1" data-path= ' + response.mainpath + ' data-text=' + response.data.files[$i].name + '><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
        for ($i = 0; $i < (response.data.folder.length); $i++) {
          $(".rigthContent").attr('data-path', response.mainpath).append('<div class="parent"><button class="children1" data-path= ' + response.mainpath + ' data-text=' + response.data.folder[$i].name + '><img src="./assets/images/folder.png" alt="">' + response.data.folder[$i].name + '</button><div class="sub-panel-2"></div></div>');
        }
      }
    });
  }
});

$(document).on("click", ".create", function () {
  var dir = $(".rigthContent").attr('data-path');
  var newName = new RegExp("^[a-zA-Z0-9]*$");//to check the input
  bootbox.prompt({
    title: "Enter the folder name correctly",
    centerVertical: true,
    callback: function (result) {
      if (result) {// enters the loop only if user have gave any value
        if (newName.test(result)) {//check the user value
          $.ajax({
            url: "./php/foldercreate.php",
            data: { method: 'newCreate', foldername: result, dir: dir },
            method: "POST",
            dataType: "json",
            success: function (response) {
              if (response.status) {//if folder is unique than enters the condition
                bootbox.alert({
                  message: "folder is created",
                  backdrop: true
                });
                if (dir == "../root") {
                  $(".sub-panel").append('<div class="parent"><button class="children" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/folder.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                  $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/folder.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                } else {
                  $(".left").find(".active:last").append('<div class="parent"><button class="children" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/folder.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                  $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/folder.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                }
              }
              else {//if folder exists than it will give an error
                bootbox.alert({
                  message: "Already exist",
                  backdrop: true
                });
              }
            }
          });
        } else {//if user value fails to pass the correct value than enter here
          bootbox.alert({
            message: "Special Characters are not allowed",
            backdrop: true
          });
        }
      }
    }
  });
});

$(document).on('click', '.children1', function (event) {
  if (!event.ctrlKey) {//this is added to notify that delete this folder/file and this condition will allow to choose multiple elements
    $(this).eq($(this).index()).addClass("deletethis").parent().siblings().children().removeClass("deletethis");
    var temp = $(this).eq($(this).index()).attr("data-text");//this will add delete class on the left side
    $('.left').find("[data-text ='" + temp + "']").addClass('deletethis').parent().siblings().children().removeClass("deletethis");
  } else {
    var temp = $(this).eq($(this).index()).attr("data-text");
    $('.left').find("[data-text ='" + temp + "']").addClass('deletethis');//this will add delete class on the left side
    $(this).eq($(this).index()).addClass("deletethis");
  }
});

$(document).on("click", ".delete", function () {
  var select = $(".rigthContent").find(".deletethis");
  var dir = [];
  var filename = $(select).attr("data-text");
  for (let i = 0; i < select.length; i++) {
    dir[i] = ($(select).eq(i).attr("data-path") == "../root") ? $(select).eq(i).attr("data-path") + "/" + $(select).eq(i).attr("data-text") : $(select).eq(i).attr("data-path") + "/" + $(select).eq(i).attr("data-text");
  }
  bootbox.confirm({
    size: "small",
    message: "<h2>Are you sure want to delete?<h2>",
    callback: function (result) {
      if (result) {
        ajaxDelCall(dir)
      }
    }
  })
  function ajaxDelCall(boxResult) {
    $.ajax({
      url: "./php/delete.php",
      dataType: "json",
      data: { method: 'newDel', filename: filename, folderpath: boxResult },
      method: "POST",
      success: function (response) {
        if (response.status) {
          $('.deletethis').parent().remove();//find the deletethis class and remove parent element
          bootbox.alert("Deleted Successfully")
        } else {
          bootbox.alert("Process failed")
        }
      }
    });
  }
});

$(document).on("click", ".add", function () {//same as create folder 
  var dir = $(".rigthContent").attr('data-path');
  var newName = new RegExp("^[a-zA-Z0-9\.\-\/]*$");
  bootbox.prompt({
    title: "Enter the file name with extension",
    callback: function (result) {
      if (result) {
        if (newName.test(result)) {
          $.ajax({
            url: "./php/filecreate.php",
            data: { method: 'newAdd', foldername: result, dir: dir },
            method: "POST",
            dataType: "json",
            success: function (response) {
              if (response.status) {
                bootbox.alert({
                  message: "file is created",
                  backdrop: true
                });
                var ext = result.split('.')[1];
                if (dir == "../root") {
                  $(".sub-panel").append('<div class="parent"><button class="children" data-path= "' + dir + '"  data-text="' + result + '"><img src="./assets/images/' + ext + '.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                  $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/' + ext + '.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                } else {
                  $(".left").find(".active:last").append('<div class="parent"><button class="children" data-path= "' + dir + '"  data-text="' + result + '"><img src="./assets/images/' + ext + '.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                  $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + dir + '" data-text="' + result + '"><img src="./assets/images/' + ext + '.png" alt="">' + result + '</button><div class="sub-panel-2"></div></div>');
                }
              }
              else {
                bootbox.alert({
                  message: "Already exist",
                  backdrop: true
                });
              }
            }
          })
        } else {
          bootbox.alert({
            message: "Special characters are not allowed",
            backdrop: true
          });
        }
      }
    }
  });
});

function paste(pasteMethod) {
  var select = $(".rigthContent").find(".deletethis");
  for (let i = 0; i < select.length; i++) {
    dir1[i] = ($(select).eq(i).attr("data-path") == "../root") ? $(select).eq(i).attr("data-path") + "/" + $(select).eq(i).attr("data-text") : $(select).eq(i).attr("data-path") + "/" + $(select).eq(i).attr("data-text");
    cutName[i] = $(select).eq(i).attr("data-text");
    click = ($(pasteMethod).hasClass('cut')) ? 'cut' : 'copy';// this will check wether click or cut is clicked
  }
  $('.paste').prop('disabled', false);//paste button enable from herer
  $('.paste').removeClass('opacitylight');//paste button opacity add
}

$(document).on("click", ".cut", function () {
  paste($(this));//pass the cut information and notify paste function that cut is clicked
});

$(document).on("click", ".copy", function () {
  paste($(this));//pass the cut information and notify paste function that cut is clicked
});

$(document).on("click", ".paste", function () {
  var newPath = $(".rigthContent").attr("data-path");
  $.ajax({
    url: "./php/paste.php",
    data: { method: 'newPaste', foldername: cutName, dir: newPath, action: click, oldpath: dir1 },
    method: "POST",
    dataType: "json",
    success: function (response) {
      if (response.status) {
        $('.paste').addClass('opacitylight');
        $(".rigthContent").children().remove();
        show(filename1 = "", newPath);
        bootbox.alert("Pasted successfully");
      } else {
        bootbox.alert("Process failed");
      }
    }
  });
});

function rename() {
  var newPath = $(".rigthContent").attr("data-path");
  var select = $(".rigthContent").find(".deletethis");
  var newName = new RegExp("^[a-zA-Z0-9\.\-\/]*$");
  dir1 = ($(select).attr("data-path") == "../root") ? $(select).attr("data-path") + "/" + $(select).attr("data-text") : $(select).attr("data-path") + "/" + $(select).attr("data-text");
  bootbox.prompt({
    title: "Enter the new name",
    centerVertical: true,
    callback: function (result) {
      if (result) {
        if (newName.test(result)) {
          $.ajax({
            url: "./php/rename.php",
            data: { method: 'newRename', foldername: result, oldpath: dir1 },
            method: "POST",
            dataType: "json",
            success: function (response) {
              if (response.status) {
                bootbox.alert({
                  message: "Renamed successfully",
                  backdrop: true
                });
                $(".rigthContent").children().remove();
                show(filename1 = "", newPath);
              } else {
                bootbox.alert({
                  message: "Renamed faild",
                  backdrop: true
                });
              }
            }
          })
        }
      }
    }
  });
}

var dropZone;// variable to access submit button globally
$(document).on("click", ".upload", function () {
  var currentPath = $('.rigthContent').attr('data-path');
  dialog = bootbox.dialog({// we are using bootbox dialogbox to add the files
    title: "Upload Files",
    // for upload file  without dropzone// message: '<form id="form" class="dropzone" method="post" enctype="multipart/form-data"><div class="form-group"><input type="file"  id="myFile" name="filename[]" multiple/><input type="button" id="submitbootbox" value="submit"></div></form>',
    message: '<form id="form" class="dropzone" method="post" enctype="multipart/form-data"></form><input type="button" id="submitbootbox" value="submit"><div class="dzError"></div>',
  });

  Dropzone.autoDiscover = false;
  dropZone = new Dropzone(".dropzone", {//dropzone is made from here, just you have to add dropzone class in form
    url: "./php/upload.php",
    addRemoveLinks: true,
    uploadMultiple: true,
    parallelUploads: 10,
    maxFiles: 10,
    acceptedFiles: ".jpeg,.jpg,.png,.zip,.XLS,.txt,.gif",//specifies the extention
    autoProcessQueue: false,
    init: function () {
      this.on("sendingmultiple", function (file, xhr, formData) {
        formData.append('currentPath', currentPath);
      });
    },
    successmultiple: function (file, response) {//seccessmultiple flies to get multiple file togther, else it will make a loop and append 2 times
      if (response.error == false) {
        bootbox.alert({
          message: "uploaded successfully",
          size: 'small'
        });
        for ($i = 0; $i < (response.data.files.length); $i++) {
          var ext = (response.data.files[$i].name).split('.')[1];
          if (currentPath == "../root") {
            $(".sub-panel").append('<div class="parent"><button class="children" data-path= "' + currentPath + '"  data-text="' + response.data.files[$i].name + '"><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
            $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + currentPath + '" data-text="' + response.data.files[$i].name + '"><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
          } else {
            $(".left").find(".active:last").append('<div class="parent"><button class="children" data-path= "' + currentPath + '"  data-text="' + response.data.files[$i].name + '"><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
            $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "' + currentPath + '" data-text="' + response.data.files[$i].name + '"><img src="./assets/images/' + ext + '.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
          }
        }
      }
      if (response.error == true) {
        bootbox.alert({
          message: "Already Exist",
          size: 'small'
        });
      }
    },
  });
});

//for upload file with dropzone
$(document).on("click", "#submitbootbox", function (e) {
  e.preventDefault();
  if(dropZone.getRejectedFiles().length>0){//check if user have selected any wrong file forcefully
    $('.dzError').text('File Type not allowd');
  }else if(dropZone.getAcceptedFiles().length==0 && dropZone.getRejectedFiles().length==0){//check if user have not selected files
    $('.dzError').text('choose atleast one file');
  }else{//if evreything is perfect than operation will perform successfully
    dialog.modal('hide');
    dropZone.processQueue();
  }
  
  // var currentPath = $('.rigthContent').attr('data-path');
  // var formData = new FormData($('#form')[0]);
  // formData.append('currentPath', currentPath);
  // $.ajax({
  //   url: "./php/upload.php",
  //   data:formData,
  //   method: "POST",
  //   dataType: "json",
  //   processData: false,
  //   contentType: false,
  //   success: function (response) {
  //     if (response.error) {
  //       bootbox.alert({
  //         message: "Already Exist",
  //         size: 'small'
  //     });
  //   } else {
  //     for($i=0; $i< (response.data.files.length); $i++){
  //      var ext = (response.data.files[$i].name).split('.')[1];
  //      if(currentPath == "../root"){
  //        $(".sub-panel").append('<div class="parent"><button class="children" data-path= "'+currentPath+'"  data-text="'+response.data.files[$i].name+'"><img src="./assets/images/'+ext+'.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
  //        $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "'+currentPath+'" data-text="'+response.data.files[$i].name+'"><img src="./assets/images/'+ext+'.png" alt="">' +response.data.files[$i].name+'</button><div class="sub-panel-2"></div></div>'); 
  //      }else{
  //        $(".left").find(".active:last").append('<div class="parent"><button class="children" data-path= "'+currentPath+'"  data-text="'+response.data.files[$i].name+'"><img src="./assets/images/'+ext+'.png" alt="">' + response.data.files[$i].name + '</button><div class="sub-panel-2"></div></div>');
  //        $(".rigthContent").append('<div class="parent"><button class="children1" data-path= "'+currentPath+'" data-text="'+response.data.files[$i].name+'"><img src="./assets/images/'+ext+'.png" alt="">' +response.data.files[$i].name+'</button><div class="sub-panel-2"></div></div>'); 
  //      }
  //     }
  //       bootbox.alert({
  //           message: "Files uploaded successfully",
  //           centerVertical: true,
  //           size: 'small'
  //       });
  //   }
  //   }
  // })
  // dialog.modal('hide');
});

//custom menu on right click
$(document).bind("contextmenu", function (event) {
  event.preventDefault();
  if ($(".right").find(event.target).length > 0) {
    $(".custom-menu").finish().toggle(100).
      css({
        top: event.pageY + "px", left: event.pageX + "px"
      });
  } else {
    $(".custom-menu1").finish().toggle(100).
      css({
        top: event.pageY + "px", left: event.pageX + "px"
      });
  }
});
$(document).bind("mousedown", function (e) {
  if (!$(e.target).parents(".custom-menu").length > 0) {
    $(".custom-menu").hide(100);
  }
  if (!$(e.target).parents(".custom-menu1").length > 0) {
    $(".custom-menu1").hide(100);
  }
});
$(".custom-menu li").click(function () {
  switch ($(this).attr("data-action")) {
    case "first": $(".cut").trigger("click"); break;
    case "second": $(".copy").trigger("click"); break;
    case "third": $(".delete").trigger("click"); break;
    case "fourth": rename(); break;
  }
  $(".custom-menu").hide(100);
});
$(".custom-menu1 li").click(function () {
  switch ($(this).attr("data-action")) {
    case "first": $(".create").trigger("click"); break;
    case "second": $(".add").trigger("click"); break;
  }
  $(".custom-menu1").hide(100);
});