<?php
$dir = '../root';

function getFiles($dir){
    $res['folder'] = [];
    $res['files'] = [];
    $files = array_diff(scandir($dir), array('.','..'));

    $extenstionAllowed=['txt','jpeg','jpg','png'];
    foreach($files as $key => $value){
        $temp = [];
        $temp['name'] = $value; 
        $temp['path'] = $dir.'/'.$value;

        if(!is_dir($dir.'/'.$value)){
            if(array_search(pathinfo($dir .'/'. $value,PATHINFO_EXTENSION), $extenstionAllowed) !== false){
                $temp['type'] = $extenstionAllowed[array_search(pathinfo($dir.'/'.$value,PATHINFO_EXTENSION), $extenstionAllowed)];
                array_push($res['files'],$temp);
            }
        }else{
            $temp['type'] = 'folder';
            array_push($res['folder'],$temp);
        }
    }
    return $res;
}

if($_POST['method']=='back' && isset($_POST['method'])){
    $dir1 = (isset($_POST)) ? $_POST['dir'] : 'root';
        $dir = $dir1;

        $res = getFiles($dir);
        $res['mainpath'] = $dir;
        $response['status'] = true;
        $response['message'] = 'Deleted successfully.';
        $response['data'] = $res;
        $response['mainpath'] = $dir;
        echo json_encode($response);
}
?>