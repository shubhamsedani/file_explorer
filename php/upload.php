<?php

$currentPath = $_POST['currentPath'];
$allowedExtensions = ['jpg', 'png', 'gif', 'jpeg','zip','XLS','txt'];
$res['files'] = [];
$flag = 0; 

for($i=0; $i<count($_FILES['filename']['name']); $i++){
    $target_file = $currentPath .'/'. basename($_FILES["filename"]["name"][$i]);
    $fileExtension = strtolower(pathinfo(($_FILES['filename']['name'][$i]), PATHINFO_EXTENSION));
    
    if (!in_array($fileExtension, $allowedExtensions)) {
        $flag = 1;
        break;
    }else{
        if(file_exists($target_file)){        
            $flag = 1;
            break; 
        } else{
            $flag = 0;
            move_uploaded_file($_FILES['filename']['tmp_name'][$i],$target_file);
            $data = [
                'name' => basename($_FILES["filename"]["name"][$i]),
                'path' => $target_file . '/',
                'extention' => strtolower(pathinfo(($_FILES['filename']['name'][$i]), PATHINFO_EXTENSION))
            ];
            array_push($res['files'], $data);
        }
    }
}

if($flag == 0){
    $response['error'] = false;
    $response['data'] = $res;
    $response['message'] = 'File uploaded successfully !';
}else{
    $response['error'] = true;
    $response['data'] = $res;
    $response['message'] = 'File uploading fail !';
}

header('Content-Type: application/json');
echo json_encode($response);
?>