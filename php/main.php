<?php
$dir = '../root';

function getFiles($dir){
    $res['folder'] = [];
    $res['files'] = [];
    $files = array_diff(scandir($dir), array('.','..'));
    $extenstionAllowed=['jpg', 'png', 'gif', 'jpeg','zip','XLS','txt'];
    foreach($files as $key => $value){
        $temp = [];
        $temp['name'] = $value;
        $temp['path'] = $dir.'/'.$value;

        if(!is_dir($dir.'/'.$value)){
            if(array_search(pathinfo($dir .'/'. $value,PATHINFO_EXTENSION), $extenstionAllowed) !== false){
                $temp['type'] = $extenstionAllowed[array_search(pathinfo($dir.'/'.$value,PATHINFO_EXTENSION), $extenstionAllowed)];
                array_push($res['files'],$temp);
            }
        }else{
            $temp['type'] = 'folder';
            array_push($res['folder'],$temp);
        }
    }
    return $res;
}

if($_POST['method']=='get' && isset($_POST['method'])){
    $res = getFiles($dir);
    $response['status'] = true;
    $response['message'] = 'Folder created successfully.';
    $response['data'] = $res;
    header('Content-Type: application/json');
    echo json_encode($response);
}

if($_POST['method']=='check' && isset($_POST['method'])){
    $dir = (isset($_POST)) ? $_POST['dir'] : 'root';
    $arrayValue =isset($_POST['filename1']) ? $_POST['filename1']:'';
    if(is_dir($dir.DIRECTORY_SEPARATOR. $arrayValue)){
        if(empty($arrayValue) ) {
            $dir = ($dir=='../root') ? $dir.$arrayValue: $dir.$arrayValue ;
        }else{
            $dir = ($dir=='../root') ? $dir.'/'.$arrayValue: $dir.'/'.$arrayValue ;
        }

        $res = getFiles($dir);
        $res['mainpath'] = $dir;
        $response['status'] = true;
        $response['message'] = 'Folder created successfully.';
        $response['data'] = $res;
        $response['mainpath'] = $dir;
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}
?>