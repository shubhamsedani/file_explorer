<?php
$dir = '../root';
$flag = false;
if($_POST['method']=='newAdd' && isset($_POST['method'])){
    $newPath = $_POST['dir']."/".$_POST['foldername'];
    if(preg_match("/\.(txt|png|jpg|jpeg)$/", $_POST['foldername'])){
        if(!file_exists($newPath)) {
            fopen($newPath, "w");
            $flag = true;
        }else{
            $flag = false;
        }
    }else{
        $flag = false;
    }
    if($flag){
        $res['status'] = true;
        $res['message'] = 'File created successfully.';
    }else{
        $res['status'] = false;
        $res['message'] = 'Something went wrong.';
    }
    echo(json_encode($res));
}
?>