<?php
$dir = '../root';

function xcopy($source, $dir) {
    $files = array_diff(scandir($source),array('.','..'));
    if(is_dir($source)){
        if( !file_exists($dir) ){
            mkdir($dir,0700);
        }
    }
    if(!empty($files)){
        foreach ( $files  as $file) {
        if (!is_readable($source . '/' . $file)) continue;
            if (is_dir($source .'/' . $file) && ($file != '.') && ($file != '..') ) {
                if( !file_exists($dir . '/' . $file) ){
                    mkdir($dir . '/' . $file);
                }
                xcopy($source . '/' . $file, $dir . '/' . $file);
            } else {
                copy($source . '/' . $file, $dir . '/' . $file);
            }
        }
    }
}

if($_POST['method']=='newPaste' && isset($_POST['method'])){
    if ($_POST["action"] == "cut") {
        $foldername = $_POST["foldername"];
        $newPlace = $_POST["dir"];
        $oldpath = $_POST["oldpath"];
        $flag = 1;
            for($i=0; $i<count($oldpath); $i++){
                if(strpos($newPlace, $foldername[$i]) !== false){//if user cut a folder and paste init than give error
                    $flag = 0;
                }else{
                    $dir = ($newPlace=='../root') ? $newPlace.'/'.$foldername[$i] .'/' : $newPlace.'/'.$foldername[$i] ;
                    if (!file_exists($dir)) {
                        rename($oldpath[$i], $dir);
                    }else{
                        $flag = 0;
                        break;
                    }
            }
        }
            if ($flag==1) {
                $res['status'] = true;
                $res['message'] = 'File cut successfully.';
            }else{
                $res['status'] = false;
                $res['message'] = 'Something went wrong.';
            }
            header('Content-Type: application/json');
            echo(json_encode($res));
    }

    if ($_POST["action"] == "copy") {
        $foldername = $_POST["foldername"];
        $newPlace = $_POST["dir"];
        $oldpath = $_POST["oldpath"];
        $flag = false;
        for($i=0; $i<count($oldpath); $i++){
            $dir = ($newPlace=='../root') ? $newPlace.'/'.$foldername[$i]  : $newPlace.'/'.$foldername[$i];
            if (is_dir($oldpath[$i])){
                if(file_exists($dir)){
                    $flag = true;   
                    $j=1;                    
                    while( file_exists($dir.'-'.$j) ){                                                                                                                
                        $j++;                        
                    }   
                    $dir1 = $dir.'-'.$j;
                    xcopy($oldpath[$i], $dir1);  
                }else {
                    $flag = true;   
                    xcopy($oldpath[$i], $dir);                    
                }
            } else {     
                if(file_exists($dir)){
                    $flag = true;   
                    $j=1;                    
                    while( file_exists($dir.'-'.$j) ){                                                                                                                
                        $j++;                        
                    }   
                    $dir1 = $dir.'-'.$j;
                    copy($oldpath[$i], $dir1);   
                }else {
                    $flag = true;   
                    copy($oldpath[$i], $dir);                     
                }              
            }
        }        
        
        if($flag){
            $res['status'] = true;
            $res['message'] = 'File copied successfully.';
        }else{
            $res['status'] = false;
            $res['message'] = 'Something went wrong.';
        }
        header('Content-Type: application/json');
        echo(json_encode($res));
    }
}  
?>