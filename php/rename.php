<?php
$dir = '../root';

if($_POST['method']=='newRename' && isset($_POST['method'])){
    $foldername = $_POST["foldername"];
    $oldpath = $_POST["oldpath"];
    $flag = false;
    
    if(is_dir($oldpath)){
        if(!preg_match("/^[a-zA-Z0-9]*$/", $_POST['foldername'])){
            $flag = false;
            echo 1;
        }else{
            $last = strripos($oldpath, "/");
            $temp = substr($oldpath, 0, $last+1);
            $update = $temp . $foldername;
            rename($oldpath, $update);
            $flag = true;
        }
    }else{
        if(!preg_match("/\.(txt|png|jpg)$/", $_POST['foldername'])){
            $flag = false;
            echo 2;
        }else{
            $last = strripos($oldpath, "/");
            $temp = substr($oldpath, 0, $last+1);
            $update = $temp . $foldername;
            rename($oldpath, $update);
            $flag = true;
        }
    }
    if($flag){
        $res['status'] = true;
        $res['message'] = 'File copied successfully.';
    }else{
        $res['status'] = false;
        $res['message'] = 'Something went wrong.';
    }
    echo(json_encode($res));
    // print_r(json_encode(array($message)));
}
?>